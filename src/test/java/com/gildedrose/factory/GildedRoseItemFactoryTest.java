package com.gildedrose.factory;

import com.gildedrose.Item;
import com.gildedrose.item.*;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GildedRoseItemFactoryTest {

    List<Item> items;


    @Before
    public void setUp(){
        items = new ArrayList<>();
        items.add(new Item("camembert", 3, 15));
        items.add(new Item("spaghetti", 20, 55));
        items.add(new Item("aged brie", 6, 36));
        items.add(new Item("apple", 7, 34));
        items.add(new Item("banana", 4, 19));
        items.add(new Item("beer", 20, 45));
        items.add(new Item("backstage passes", 15, 33));
        items.add(new Item("corn flakes", 16, 47));
        items.add(new Item("cheese", 25, 45));
        items.add(new Item("milk", 5, 20));
        items.add(new Item("sulfuras", 7, 77));
        items.add(new Item("french fries", 13, 37));
        items.add(new Item("conjured", 5, 19));
        items.add(new Item("chips", 0, 0));
        items.add(new Item("soda", 0, 0));
        items.add(new Item("champagne", 0, 0));

    }

    @Test
    public void getGildedRoseItem() {
        assertEquals(items.get(0).name, GildedRoseItemFactory.getGildedRoseItem(items.get(0)).getName());
        assertEquals(items.get(1).name, GildedRoseItemFactory.getGildedRoseItem(items.get(1)).getName());
        assertEquals(items.get(2).name, GildedRoseItemFactory.getGildedRoseItem(items.get(2)).getName());
        assertEquals(items.get(3).name, GildedRoseItemFactory.getGildedRoseItem(items.get(3)).getName());
        assertEquals(items.get(4).name, GildedRoseItemFactory.getGildedRoseItem(items.get(4)).getName());
        assertEquals(items.get(5).name, GildedRoseItemFactory.getGildedRoseItem(items.get(5)).getName());
        assertEquals(items.get(6).name, GildedRoseItemFactory.getGildedRoseItem(items.get(6)).getName());
        assertEquals(items.get(7).name, GildedRoseItemFactory.getGildedRoseItem(items.get(7)).getName());
        assertEquals(items.get(8).name, GildedRoseItemFactory.getGildedRoseItem(items.get(8)).getName());
        assertEquals(items.get(9).name, GildedRoseItemFactory.getGildedRoseItem(items.get(9)).getName());
        assertEquals(items.get(10).name, GildedRoseItemFactory.getGildedRoseItem(items.get(10)).getName());
        assertEquals(items.get(11).name, GildedRoseItemFactory.getGildedRoseItem(items.get(11)).getName());
        assertEquals(items.get(12).name, GildedRoseItemFactory.getGildedRoseItem(items.get(12)).getName());
        assertEquals(items.get(13).name, GildedRoseItemFactory.getGildedRoseItem(items.get(13)).getName());
        assertEquals(items.get(14).name, GildedRoseItemFactory.getGildedRoseItem(items.get(14)).getName());
        assertEquals(items.get(15).name, GildedRoseItemFactory.getGildedRoseItem(items.get(15)).getName());
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(0)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(1)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(2)), IsInstanceOf.instanceOf(AgedBrieItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(3)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(4)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(5)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(6)), IsInstanceOf.instanceOf(BackstageItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(7)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(8)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(9)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(10)), IsInstanceOf.instanceOf(SulfurasItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(11)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(12)), IsInstanceOf.instanceOf(ConjuredItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(13)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(14)), IsInstanceOf.instanceOf(DefaultItem.class));
        assertThat(GildedRoseItemFactory.getGildedRoseItem(items.get(15)), IsInstanceOf.instanceOf(DefaultItem.class));

    }
}