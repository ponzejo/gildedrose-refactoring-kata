package com.gildedrose.item;

import com.gildedrose.Item;
import org.junit.Test;

import static org.junit.Assert.*;

public class BackstageItemTest {

    BackstageItem backstageItem = new BackstageItem(new Item("backstage passes", 7, 7));

    @Test
    public void updateQuality() {
        backstageItem.updateQuality();
        assertEquals("backstage passes", backstageItem.name);
        assertEquals(6, backstageItem.sellIn);
        assertEquals(10, backstageItem.quality);
    }
}