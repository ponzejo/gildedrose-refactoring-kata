package com.gildedrose.item;

import com.gildedrose.Item;
import org.junit.Test;

import static org.junit.Assert.*;

public class AgedBrieItemTest {

    AgedBrieItem agedBrieItem = new AgedBrieItem(new Item("aged brie", 5, 10));

    @Test
    public void updateQuality() {
        agedBrieItem.updateQuality();
        assertEquals("aged brie", agedBrieItem.name);
        assertEquals(4, agedBrieItem.sellIn);
        assertEquals(11, agedBrieItem.quality);
    }
}