package com.gildedrose.item;

import com.gildedrose.Item;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConjuredItemTest {

    ConjuredItem conjuredItem = new ConjuredItem(new Item("conjured", 10, 38));

    @Test
    public void updateQuality() {
        conjuredItem.updateQuality();
        assertEquals("conjured", conjuredItem.name);
        assertEquals(9, conjuredItem.sellIn);
        assertEquals(36, conjuredItem.quality);
    }
}