package com.gildedrose;

import static org.junit.Assert.*;

import org.junit.Test;

public class GildedRoseTest {

    @Test
    public void updateQuality() {
        Item[] items = new Item[] { new Item("aged brie", 0, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("aged brie", app.getGildedRoseItems().get(0).getName());
        assertEquals(-1, app.getGildedRoseItems().get(0).getSellIn());
        assertEquals(1, app.getGildedRoseItems().get(0).getQuality());
    }

}
