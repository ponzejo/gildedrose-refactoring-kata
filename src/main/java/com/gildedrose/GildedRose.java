package com.gildedrose;

import com.gildedrose.factory.GildedRoseItemFactory;
import com.gildedrose.item.GildedRoseItem;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


class GildedRose {
    Item[] items;

    @Getter
    private List<GildedRoseItem> gildedRoseItems = new ArrayList<>();

    public GildedRose(Item[] items) {
        this.items = items;
        for (Item i : items) {
            gildedRoseItems.add(GildedRoseItemFactory.getGildedRoseItem(i));
        }
    }

    public void updateQuality() {
        for(GildedRoseItem gildedRoseItem : gildedRoseItems){
            gildedRoseItem.updateQuality();
        }
    }

    public String toString(int days) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < days; i++) {
            stringBuilder.append(String.format("-------- day %s --------\n", i));
            stringBuilder.append(String.format("name, sellIn, quality\n"));
            for (GildedRoseItem item : gildedRoseItems) {
                stringBuilder.append(item.toString() + "\n");
            }
            stringBuilder.append("\n");
            updateQuality();
        }
        return stringBuilder.toString();
    }


}