package com.gildedrose.item;

import com.gildedrose.Item;

public class SulfurasItem extends GildedRoseItem {

    public SulfurasItem(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        //never get old
    }
}
