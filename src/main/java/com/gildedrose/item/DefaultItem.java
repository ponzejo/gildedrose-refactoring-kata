package com.gildedrose.item;

import com.gildedrose.Item;

public class DefaultItem extends GildedRoseItem {

    public DefaultItem(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        if (quality > 0) quality--;

    }
}
