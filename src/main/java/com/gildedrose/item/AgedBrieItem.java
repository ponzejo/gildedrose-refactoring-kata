package com.gildedrose.item;

import com.gildedrose.Item;

public class AgedBrieItem extends GildedRoseItem {

    public AgedBrieItem(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        if (quality < 50) quality++;
    }
}
