package com.gildedrose.item;

import com.gildedrose.Item;
import lombok.Data;

@Data
public abstract class GildedRoseItem {

    protected String name;
    protected int sellIn;
    protected int quality;

    public GildedRoseItem(Item item) {
        name = item.name;
        sellIn = item.sellIn;
        quality = item.quality;
    }

    public void updateQuality() {
        //see other items for specific behaviour
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }
}
