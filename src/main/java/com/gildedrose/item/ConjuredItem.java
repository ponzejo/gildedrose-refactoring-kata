package com.gildedrose.item;

import com.gildedrose.Item;

public class ConjuredItem extends GildedRoseItem {

    public ConjuredItem(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        if (quality > 0) quality--;
        if (quality > 0) quality--;
    }
}
