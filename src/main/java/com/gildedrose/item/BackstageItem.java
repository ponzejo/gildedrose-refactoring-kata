package com.gildedrose.item;

import com.gildedrose.Item;

public class BackstageItem extends GildedRoseItem {

    public BackstageItem(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        sellIn--;
        if (sellIn > -1 && sellIn < 6) {
            if (quality < 50) quality++;
            if (quality < 50) quality++;
        } else if (sellIn > 5 && sellIn < 11) {
            if (quality < 50) quality++;
            if (quality < 50) quality++;
            if (quality < 50) quality++;
        } else {
            if (quality < 50) quality++;
        }
        if (sellIn < 0) quality = 0;
    }
}
