package com.gildedrose.factory;

import com.gildedrose.Item;
import com.gildedrose.item.*;

public class GildedRoseItemFactory {

    public static GildedRoseItem getGildedRoseItem(Item item) {

        GildedRoseItem gildedRoseItem;

        if(item.name.toLowerCase().contains("aged brie"))
            gildedRoseItem = new AgedBrieItem(item);
        else if (item.name.toLowerCase().contains("backstage passes"))
            gildedRoseItem = new BackstageItem(item);
        else if (item.name.toLowerCase().contains("sulfuras"))
            gildedRoseItem = new SulfurasItem(item);
        else if (item.name.toLowerCase().contains("conjured"))
            gildedRoseItem = new ConjuredItem(item);
        else
            gildedRoseItem = new DefaultItem(item);

        return gildedRoseItem;

    }
}
